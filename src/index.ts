import app from './app';
import dotenv from 'dotenv';
import {connection} from './database'

//config environment
dotenv.config();

const port = process.env.PORT || 3000;

async function main() {
    connection();
    app.listen(port, () => {
        console.log(`server started at http://localhost:${ port }` );
    })
}

main();