import axios from 'axios';
import dotenv from 'dotenv';

dotenv.config();

export class selfGeneratedImages {
    url: string;
    
    constructor(){
        this.url = process.env.host || '';
    }

    async getImageDescription(experience: string): Promise<[{}]> {
        return new Promise(async (resolve, reject) =>{
            await axios({
                method: 'get',
                url: this.url,
                headers: {
                    experience: experience.toUpperCase()
                }
            })
            .then(response => {
                resolve(response.data);
            }).catch(err => {reject(err)});
        })
        }
}