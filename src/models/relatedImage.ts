export interface IRelatedImage {
    _id: string,
    name: string,
    experience: string
    address: string
}