import {Schema, model, Document} from 'mongoose';

const schema = new Schema({
    title: String,
    description: String,
    interactiveroom: String,
    relatedimage: String,
    image: String
})

export interface IExperiences extends Document {
    title: string,
    description: string,
    interactiveroom: string,
    relatedimage: string,
    image: string
}

export default model<IExperiences>('experience', schema);