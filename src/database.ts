import mongoose from 'mongoose';
import dotenv from 'dotenv';

dotenv.config();

const pss = process.env.db_PASS;
const name = process.env.db_NAME;

export async function connection() {
    console.log('initializing connection to database');
    await mongoose.connect(`mongodb+srv://db_dbuser:${pss}@cluster0-s8ajf.mongodb.net/${name}?retryWrites=true&w=majority`, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => {
        console.log('database is connected');
    })
    .catch(err => {
        console.log('connection to database failed: \n', err);
    })
}