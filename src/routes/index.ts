import {Router} from 'express';

import {createExperience, getExperiences, updateExperience} from '../controllers/experiences.controller'
import {removeExperience, getExperience, getByRooms} from '../controllers/experiences.controller'

const router = Router();

router.route('/')
    .post(createExperience)
    .get(getExperiences)

router.route('/:id')
    .put(updateExperience)
    .delete(removeExperience)
    .get(getExperience)

router.route('/salas/:nombre')
    .get(getByRooms)

export default router;