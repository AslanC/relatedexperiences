import Experiences from '../models/experiences';
import {IExperiences} from '../models/experiences';


export async function createDocument(obj: any): Promise<{}> {
    
    return new Promise<{}>((resolve, reject) =>{
        const newExperiences = new Experiences({
            title: obj.title,
            description: obj.description,
            interactiveroom: obj.interactiveroom.toUpperCase(),
            relatedimage: '',
            image: obj.image
        });

        resolve(newExperiences);
    });
}

export async function saveDocument(newFile:{}): Promise<{}> {
    const doc = new Experiences(newFile);
    let json: {};
    try {
        await doc.save();
        json = ({
            message: 'la experiencia se ha guardado con éxito',
            error: '',
            doc
        });
    } catch (err) {
        console.log(err);
        json = ({
            message: '',
            error: 'Hubo un error al guardar',
        });
    }
    return json;
}

export function findDocument(id:string) : Promise<IExperiences> {
    return new Promise( async(resolve, reject) => {
        try {
           await Experiences.findById(id, (err, doc) => {
                if(err) {reject({
                    message:"Hubo un error con la busqueda"
                })}

                const document = doc as IExperiences;
                resolve(document);
            })
        } catch (error) {
            reject({
                message: 'Hubo un error en al conectar',
                err: error
            });
        }
    })
    
}

export function findOneDocument(id:string) : Promise<Boolean> {
    let res = false;

    return new Promise( async(resolve, reject) => {
        try {
           await Experiences.findById(id, err => {
                if(err) {reject(false)}
                resolve(true);
            })
        } catch (error) {
            reject(false);
        }
    })
    
}

export function findDocuments(): Promise<any> {
    return new Promise(async (resolve, reject) => {              
        await Experiences.find({}, ((err, model) => {
            if(err) reject(err);
            resolve(model);
        }));
    });
}

export function updateDocument(id:string, img:{}): Promise<{}> {
    const filter = {_id: id};

    return new Promise(async(resolve, reject) => {
        try {
            await Experiences.findByIdAndUpdate(filter,img,
            {new:true}).then((result) => {
                
                console.log(result);
                resolve({
                    succes: true,
                    message: result
                })
            }).catch(err => {
                console.log(err);
                reject({
                    succes: false,
                    message: err
                })
            })
        } catch (error) {
            console.log(error);
            reject({
                succes: false,
                message: error
            })
        }
    })
}

export function deleteDocument(id: string): Promise<{}>{
    return new Promise(async(resolve, reject) => {
        try {
            const doc = await Experiences.findByIdAndRemove(id);
            if(doc) {
                resolve({
                    succes: true,
                    message: 'se eliminó con éxito',
                    document: doc
                })
            }

        } catch (error) {
            reject({
                succes: false,
                id: `no se encontró el id ${id}`,
                message: error
            })
        }
    })
}

export function experiencesFiltered(roomName: string): Promise<{}> {
    return new Promise(async(resolve, reject) => {
        try {
            await Experiences.find({"interactiveroom" : {$regex: '.*' + roomName + '.*'}},
            (err, data) =>{ 
                if(err) reject(err);
                resolve(data);
            })
        } catch (error) {
            reject({
                succes: false,
                message: error
            })
        }
    });
}