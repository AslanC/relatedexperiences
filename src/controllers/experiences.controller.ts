import { Request, Response } from 'express';

import { createDocument, saveDocument, findDocuments, findOneDocument, updateDocument } from '../libs/documents';
import { deleteDocument, findDocument, experiencesFiltered } from '../libs/documents';
import { selfGeneratedImages } from '../services/services'

import {IExperiences} from '../models/experiences';
import {IRelatedImage} from '../models/relatedImage'

export function createExperience(req: Request, res: Response): Promise<Response> {
    return new Promise(async(resolve, reject) => {
        const response = await funResponse(req.body).then((doc) => {
            return doc;
        }).catch((err) =>{
            return err;
        });
       res.send(response);
    });
}

export function getExperiences(req: Request, res:Response): Promise<Response> {
    console.log(req.params);
    return new Promise(async(resolve, reject) => {
        const response = await findDocuments().then((doc) => {
            return doc;
        }).catch((err) => {
            return err;
        })
        res.send(response);
    })
}

export async function updateExperience(req: Request, res: Response): Promise<Response> {
    try {
        
        const id = req.params.id !== undefined ? req.params.id.toString() : '';
        const obj = req.body;
        const doc = await findOneDocument(id);
        
        if (!doc) {
            return res.send({ message: 'al parecer el archivo no existe' });
        }

        const data = await updateDocument(id, obj).then((result) => 
        {
            return result;
        }).catch(err => {
            return err;
        })

        return res.send(data);

    } catch (error) {
        console.log(error);
        return res.send(error);
    }
}

export async function removeExperience(req: Request, res: Response): Promise<Response> {
    try {
        const id = req.params.id !== undefined ? req.params.id.toString() : '';
        const response = await deleteDocument(id);

        return res.send(response);
    } catch (error) {
        return res.send(error);
    }
}

export async function getExperience(req: Request, res:Response): Promise<Response> {
    try {
        const id = req.params.id?.toString();
        let response = await findDocument(id);
        let image = await addImagesExperiences(response.title);
        let imageExperience = image as IRelatedImage
        
        if (imageExperience) {
            response.image = imageExperience.address;
        }

        response.relatedimage = imageExperience.address;
        return res.send(response);
        
    } catch (error) {
        return res.send(error);
    }
}

export async function getByRooms(req: Request, res:Response): Promise<Response> {
    const name = req.params.nombre !== undefined ? req.params.nombre.toString() : ''; //name
    const response = await experiencesFiltered(name);
    return res.send(response);
}

async function funResponse(obj:{}): Promise<{}>{
    
    let response: {};

    return new Promise(async(resolve, reject) => {
        try {

            const document = await createDocument(obj);
            response = await saveDocument(document);
            resolve(response);

        } catch (error) {
            reject(error);
        }
    });
}

async function addImagesExperiences(experience: string): Promise<{}> {
    try {
        const generatorImage = new selfGeneratedImages();
        const image = await generatorImage.getImageDescription(experience);
        const r = image[Math.floor(Math.random() * image.length)] as IRelatedImage; 
        
        return r;
    } catch (error) {
        const message = {
            err: true,
            error: error
        }
        console.log(message);
        throw message; 
    }
}