import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import path from 'path';

import router from './routes/index';

const app = express();

app.use(cors());

//middlewares
app.use(morgan('dev'));
app.use(express.json());

//routes
app.use('/api', router);

export default app;